# coding: utf-8

from random import choice, randint, sample

import names

from actions import HipHopDance, RnBDance, ElectroDance, HouseDance, PopDance
from music import Genre, Music
from night_club import NightClub
from person import Gender, Person

if __name__ == "__main__":
    persons_count = 10
    tracks_count = 20

    persons = []
    tracks = []

    genres = tuple(Genre)
    genders = tuple(Gender)
    all_dance_skills = (
        HipHopDance(),
        RnBDance(),
        ElectroDance(),
        HouseDance(),
        PopDance()
    )

    for i in range(tracks_count):
        tracks.append(Music(choice(genres)))

    romashka_club = NightClub(tracks)

    for i in range(persons_count):
        gender = choice(genders)
        name = names.get_full_name(gender=gender.value)
        person_skills = sample(all_dance_skills, randint(0, len(all_dance_skills)))
        person = Person(name, gender, person_skills)
        romashka_club.accept(person)
    romashka_club.begin()
