# coding: utf-8
from enum import Enum
from typing import List

from actions import Dance, Action
from music import Music
from observer import Observer


class Gender(Enum):
    Female = 0
    Male = 1


class Person(Observer):
    """

    Человек, посетитель ночного клуба
    """

    def __init__(self, name: str, gender: Gender, dance_skills: List[Dance]):
        self.name = name
        self.gender = gender
        self.dance_skills = dance_skills
        self.action = Action(dance_skills)

    def __str__(self, *args, **kwargs):
        if self.dance_skills:
            skills_string = "умеет танцевать {}".format(','.join(map(str, self.dance_skills)))
        else:
            skills_string = "ничего не умеет танцевать"
        return "{} {}".format(self.name, skills_string)

    def update(self, track: Music, *args, **kwargs):
        print(self)
        super().update(track)
        self.action.update(track)
