# coding: utf-8

from abc import ABCMeta, abstractmethod


class Observer(metaclass=ABCMeta):
    """

    Абстрактный наблюдатель
    """

    @abstractmethod
    def update(self, *args, **kwargs):
        """

        Получение сообщения
        """


class Subject(metaclass=ABCMeta):
    """

    Абстрактный субъект
    """

    def __init__(self):
        self.observers = []

    def add_observer(self, observer: Observer):
        """

        Добавление нового наблюдателя
        """
        self.observers.append(observer)

    def remove_observer(self, observer: Observer):
        """

        Удаление наблюдателя
        """
        self.observers.remove(observer)

    def notify_observers(self, *args, **kwargs):
        """

        Оповещение всех подписанных наблюдателей
        """
        for observer in self.observers:
            observer.update(*args, **kwargs)
