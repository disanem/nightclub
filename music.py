# coding: utf-8
from enum import Enum


class Genre(Enum):
    RnB = 'RnB'
    ElectroHouse = 'ElectroHouse'
    Pop = 'Pop'


class Music:
    """

    Музыкальный трек
    """

    def __init__(self, genre: Genre):
        self.genre = genre
