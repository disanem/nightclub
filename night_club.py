# coding: utf-8
from typing import List

from music import Music
from observer import Subject, Observer


class NightClub(Subject):
    def __init__(self, track_list: List[Music]):
        super().__init__()
        self.track_list = track_list

    def set_track(self, track: Music):
        track_index = self.track_list.index(track)
        print("-----------------------------------------------------------------------------------------------")
        print("Играет композиция номер {}, из жанра {}".format(track_index, track.genre.name))
        self.notify_observers(track)

    def accept(self, person: Observer):
        self.add_observer(person)

    def begin(self):
        for track in self.track_list:
            self.set_track(track)
