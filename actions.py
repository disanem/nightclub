# coding: utf-8
from abc import ABCMeta, abstractmethod, abstractproperty
from random import sample
from typing import List, DefaultDict

from music import Genre, Music


class Drink:
    """

    Распитие напитка в баре
    """

    def __init__(self, beverage: str):
        self.beverage = beverage

    def do(self):
        print('Пьет напиток: {}'.format(self.beverage))


class Dance(metaclass=ABCMeta):
    """

    Абстрактный танец
    """

    def __init__(self):
        self.name = 'Танец'

    def __str__(self):
        return self.name

    def do(self):
        """

        Танцевальные движения
        """
        self.move_arms()
        self.move_head()
        self.move_body()
        self.move_legs()

    @abstractproperty
    def genre(self):
        """

        Жанры музыки, под которые можно танцевать этот танец
        """

    @abstractmethod
    def move_body(self):
        """

        Движение туловищем
        """

    @abstractmethod
    def move_head(self):
        """

        Движение головой
        """

    @abstractmethod
    def move_arms(self):
        """

        Движение руками
        """

    @abstractmethod
    def move_legs(self):
        """

        Движение ногами
        """


class Action:
    """

    Действие посетителя
    """

    def __init__(self, dance_skills: List[Dance]):
        self.default_action = Drink('водка')
        self.dance_skills = dance_skills

        self.genre_dance_map = DefaultDict(set)
        for dance in dance_skills:
            self.genre_dance_map[dance.genre].add(dance)

    def update(self, track: Music):
        """

        Обновление текущего действия
        """
        self.get_current_action(track.genre).do()

    def get_current_action(self, genre):
        """

        Получение танца в зависимости от жанра.
        Note:
            Нет сведений о приоритетности танцев, для одного жанра. Берем рандомный.
        Return
        """
        return sample(self.genre_dance_map.get(genre, {self.default_action}), 1)[0]


class HipHopDance(Dance):
    """

    Танец Hip-Hop
    """

    def __init__(self):
        super().__init__()
        self.name = "Hip-Hop"

    def move_arms(self):
        print("Руки согнуты в локтях")

    def move_head(self):
        print("Головой вперед-назад")

    def move_legs(self):
        print("Ноги в полу-присяде")

    def move_body(self):
        print("Покачивания телом вперед-назад")

    @property
    def genre(self):
        return Genre.RnB


class RnBDance(Dance):
    """

    Танец R'n'B, пока нет описания движений
    """

    def __init__(self):
        super().__init__()
        self.name = "R'n'B"

    def move_arms(self):
        print("Движения руками настолько виртуозные, что не передать словами")

    def move_head(self):
        print("Движения головой настолько виртуозные, что не передать словами")

    def move_legs(self):
        print("Движения ногами настолько виртуозные, что не передать словами")

    def move_body(self):
        print("Движения телом настолько виртуозные, что не передать словами")

    @property
    def genre(self):
        return Genre.RnB


class ElectroDance(Dance):
    """

    Танец Electro dance
    """

    def __init__(self):
        super().__init__()
        self.name = "Electro dance"

    def move_arms(self):
        print("Круговые движения - вращения руками")

    def move_head(self):
        print("Почти нет движения головой")

    def move_legs(self):
        print("Ноги двигаются в ритме")

    def move_body(self):
        print("Покачивания туловищем вперед-назад")

    @property
    def genre(self):
        return Genre.ElectroHouse


class HouseDance(Dance):
    """

    Танец House, пока нет описания движений
    """

    def __init__(self):
        super().__init__()
        self.name = "House"

    def move_arms(self):
        print("Движения руками настолько виртуозные, что не передать словами")

    def move_head(self):
        print("Движения головой настолько виртуозные, что не передать словами")

    def move_legs(self):
        print("Движения ногами настолько виртуозные, что не передать словами")

    def move_body(self):
        print("Движения телом настолько виртуозные, что не передать словами")

    @property
    def genre(self):
        return Genre.ElectroHouse


class PopDance(Dance):
    """
    Pop - танец
    """

    def __init__(self):
        super().__init__()
        self.name = "Pop"

    def move_arms(self):
        print("В основном плавные движения руками")

    def move_head(self):
        print("В основном плавные движения головой")

    def move_legs(self):
        print("В основном плавные движения ногами")

    def move_body(self):
        print("В основном плавные движения туловищем")

    @property
    def genre(self):
        return Genre.Pop
